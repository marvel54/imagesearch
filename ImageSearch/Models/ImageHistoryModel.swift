//
//  ImageHistoryModel.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class ImageHistoryModel: Object {

    @objc dynamic var previewWidth = -1
    @objc dynamic var previewHeight = -1
    @objc dynamic var previewURL = ""
    
    @objc dynamic var date = Date()
    
    @objc dynamic var uniqueId = ""
    @objc dynamic var searchedWord = ""
    
    static func create(json: JSON, searchedWord: String) -> ImageHistoryModel{
        let object = ImageHistoryModel()
        object.previewHeight = json["previewHeight"].int ?? -1
        object.previewWidth = json["previewWidth"].int ?? -1
        object.previewURL = json["previewURL"].stringValue
        object.uniqueId = "\(object.date.timeIntervalSince1970)"
        object.searchedWord = searchedWord
        return object
    }
    
    override class func primaryKey() -> String? {
        return "uniqueId"
    }
}
