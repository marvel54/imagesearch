//
//  LocalService.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit
import RealmSwift
//import Realm

class LocalService {

    static func addHistory(imageHistory: ImageHistoryModel){
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(imageHistory, update: true)
        }
    }
    
    static func getHistory() -> [ImageHistoryModel]{
        let realm = try! Realm()
        let array = Array(realm.objects(ImageHistoryModel.self))
        return array.sorted(by: { $0.date.timeIntervalSince1970 > $1.date.timeIntervalSince1970})
    }
    
    static func deleteHistory(imageHistory: ImageHistoryModel){
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(imageHistory)
        }
    }
}
