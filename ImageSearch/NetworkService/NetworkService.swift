//
//  NetworkService.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit
import SwiftyJSON

class NetworkService: NSObject {
    let apiKey = "10914720-e81cde306b6ed7ca0a84f8c60"
    let mainUrl = "https://pixabay.com/api"
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    func searchImage(text: String, completion: @escaping (JSON, Bool)->Void) {
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: mainUrl) {
            urlComponents.query = "key=\(apiKey)&q=\(text)&per_page=3"
            
            guard let url = urlComponents.url else { return }
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                if let error = error {
                    print("DataTask error: " + error.localizedDescription + "\n")
                    DispatchQueue.main.async {
                        completion(JSON.null, false)
                    }
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    let json = JSON(data)
                    DispatchQueue.main.async {
                        completion(json, true)
                    }
                }
            }
            dataTask?.resume()
        }
    }
    
    func downloadImage(url: String, completion: @escaping (UIImage?)->Void){
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: url) {
            guard let url = urlComponents.url else { return }
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let image = UIImage(data: data){
                        DispatchQueue.main.async {
                            completion(image)
                        }
                }else{
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                }
            }
            dataTask?.resume()
        }
    }
}
