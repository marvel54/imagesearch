//
//  SearchPresenter.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SearchHistoryDelegate{
    func historyDidUpdate(imageHistoryArray: [ImageHistoryModel])
    func historyDidUpdate(imageHistory: ImageHistoryModel)
    func showError()
}

class SearchPresenter: NSObject {

    private var delegate: SearchHistoryDelegate
    
    init(delegate: SearchHistoryDelegate) {
        self.delegate = delegate
        super.init()
        self.showHistory()
    }
    
    func searchImage(text: String){
        NetworkService().searchImage(text: text) { (json, isComplete) in
            if isComplete, let firstImageJson = json["hits"].arrayValue.first{
                let imageHistoryObject = ImageHistoryModel.create(json: firstImageJson, searchedWord: text)
                LocalService.addHistory(imageHistory: imageHistoryObject)
                self.delegate.historyDidUpdate(imageHistory: imageHistoryObject)
            }else{
                self.delegate.showError()
            }
        }
    }
    
    func deleteImageHistory(_ imageHistory: ImageHistoryModel){
        LocalService.deleteHistory(imageHistory: imageHistory)
    }
    
    private func showHistory(){
        self.delegate.historyDidUpdate(imageHistoryArray: LocalService.getHistory())
    }
}
