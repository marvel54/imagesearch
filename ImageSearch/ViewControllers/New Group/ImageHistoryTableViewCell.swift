//
//  ImageHistoryTableViewCell.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit

class ImageHistoryTableViewCell: UITableViewCell {

    
    private var photoImageView = UIImageView()
    private var searchedTextLabel = UILabel()
    private var newContentView = UIView()
    
    private var photoImageViewHeight: NSLayoutConstraint!
    private var photoImageViewWidth: NSLayoutConstraint!
    
    func setup(height: Int, width: Int, searchedWord: String){
        self.photoImageViewWidth.constant = CGFloat(width)
        self.photoImageViewHeight.constant = CGFloat(height)
        self.searchedTextLabel.text = searchedWord
        self.layoutIfNeeded()
    }
    
    func setup(image: UIImage?){
        DispatchQueue.main.async {
            self.photoImageView.image = image
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addNewContentView()
        self.addImageView()
        self.addSearchedTextLabel()
        self.additionalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func additionalSetup(){
        self.selectionStyle = .none
        self.photoImageView.contentMode = .center
    }
    
    private func addNewContentView(){
        self.contentView.addSubview(newContentView)
        newContentView.backgroundColor = .clear
        newContentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            newContentView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
            newContentView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
            newContentView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0),
            newContentView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            ])
    }
    
    private func addImageView(){
        self.newContentView.addSubview(self.photoImageView)
        self.photoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let left = NSLayoutConstraint(item: self.photoImageView, attribute: .left, relatedBy: .equal, toItem: self.newContentView, attribute: .left, multiplier: 1, constant: 5)
        
        let centerY = NSLayoutConstraint(item: self.photoImageView, attribute: .centerY, relatedBy: .equal, toItem: self.newContentView, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.photoImageViewWidth = NSLayoutConstraint(item: self.photoImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        self.photoImageViewHeight = NSLayoutConstraint(item: self.photoImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        self.photoImageView.addConstraints([self.photoImageViewWidth, self.photoImageViewHeight])
        self.newContentView.addConstraints([left, centerY])
    }
    
    private func addSearchedTextLabel(){
        self.newContentView.addSubview(self.searchedTextLabel)
        self.searchedTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let right = NSLayoutConstraint(item: self.searchedTextLabel, attribute: .right, relatedBy: .equal, toItem: self.newContentView, attribute: .right, multiplier: 1, constant: 5)
        
        let left = NSLayoutConstraint(item: self.searchedTextLabel, attribute: .left, relatedBy: .equal, toItem: self.photoImageView, attribute: .right, multiplier: 1, constant: 5)
        
        
        let centerY = NSLayoutConstraint(item: self.searchedTextLabel, attribute: .centerY, relatedBy: .equal, toItem: self.newContentView, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.newContentView.addConstraints([left, right, centerY])
    }

}
