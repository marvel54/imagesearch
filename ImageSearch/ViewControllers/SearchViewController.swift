//
//  SearchViewController.swift
//  ImageSearch
//
//  Created by VasylChekun on 06.12.18.
//  Copyright © 2018 VasylChekun. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {

    var presenter: SearchPresenter!
    var imageHistoryArray: [ImageHistoryModel]!
    let imageHistoryCellIdentifier = String(describing: ImageHistoryTableViewCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = SearchPresenter(delegate: self)
        
        self.setupSearchBar()
        self.setupTableView()
        self.registerCells()
    }
    
    func setupSearchBar(){
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        self.navigationItem.titleView = searchBar
    }
    
    func setupTableView(){
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()
    }
    
    func registerCells(){
        self.tableView.register(ImageHistoryTableViewCell.self, forCellReuseIdentifier: imageHistoryCellIdentifier)
    }
    
    //mark: Table View Data Source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageHistoryArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: imageHistoryCellIdentifier, for: indexPath) as! ImageHistoryTableViewCell
        let imageHistory = self.imageHistoryArray[indexPath.row]
        cell.setup(image: nil)
        NetworkService().downloadImage(url: imageHistory.previewURL) { (image) in
            cell.setup(image: image)
        }
        cell.setup(height: imageHistory.previewHeight, width: imageHistory.previewWidth, searchedWord: imageHistory.searchedWord)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.imageHistoryArray[indexPath.row].previewHeight + 10)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.tableView.beginUpdates()
            self.presenter.deleteImageHistory(self.imageHistoryArray[indexPath.row])
            self.imageHistoryArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
        }
    }
}

extension SearchViewController: SearchHistoryDelegate{
    func showError() {
        let alertController = UIAlertController(title: "Error", message: "Image not saved", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    func historyDidUpdate(imageHistoryArray: [ImageHistoryModel]){
        self.imageHistoryArray = imageHistoryArray
        self.tableView.reloadData()
    }
    
    func historyDidUpdate(imageHistory: ImageHistoryModel){
        self.tableView.beginUpdates()
        self.imageHistoryArray.insert(imageHistory, at: 0)
        self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        self.tableView.endUpdates()
    }
}

extension SearchViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.presenter.searchImage(text: searchBar.text ?? "")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
